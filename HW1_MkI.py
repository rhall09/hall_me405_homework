# -*- coding: utf-8 -*-
"""
Created on Thu Jan  7 09:32:12 2021

@author: hallm
"""

"""
Notes:
  tuple format: {pennies, nickels, dimes, quarters, dollars, fives, tens, twenties, fifties}
    
  
"""

# Define Function
def getChange (price, payment):
    while (price >= 50.00):
        price = price - 50.00
        fifts = fifts + 1
    while (price >= 20.00):
        price = price - 20.00
        twnts = twntss + 1      
    while (price >= 10.00):
        price = price - 10.00
        tens = tens + 1    
    while (price >= 5.00):
        price = price - 5.00
        fives = fives + 1 
    while (price >= 1.00):
        price = price - 1.00
        ones = ones + 1
    while (price >= 0.25):
        price = price - 0.25
        qrts = qrts + 1
    while (price >= 0.10):
        price = price - 0.10
        dims = dims + 1 
    while (price >= 0.05):
        price = price - 0.05
        nkls = nkls + 1 
    while (price >= 0.01:
        price = price - 0.01
        pns = pns + 1 
    allChng = (pns, nkls, dims, qrts, ones, fives, tens, twnts, fifts)
    if (fifts > 0):
        change = allChng
    elif (twnts > 0):
        change = allChng[1:8]
    elif (tens > 0):
        change = allChng[1:7]
    elif (fives > 0):
        change = allChng[1:6]
    elif (ones > 0):
        change = allChng[1:5]
    elif (qrts > 0):
        change = allChng[1:4]
    elif (dims > 0):
        change = allChng[1:3]
    elif (nkls > 0):
        change = allChng[1:2]
    elif (pns > 0):
        change = allChng[1]
    else:
        print('Error')
