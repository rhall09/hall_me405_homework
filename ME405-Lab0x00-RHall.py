# -*- coding: utf-8 -*-
"""
@file ME405-lab0x01-RHall.py
@brief This file contains a Finite State Machine (FSM) designed to operate a hypothetical vending machine "Vendotron."
@detail ...Not Yet implemented

Created on Thu Jan 14 08:23:38 2021

@author: hallm
"""







state = 0 # Set initial state to 0 to ensure proper initialization





while True:
    
    #State 0 - Initialization
    if state == 0:
        # Load External Functions
        import vendMath.py # Load balance computation functions
        # Initialize Keyboard
        import keyboard 

        last_key = ''

        def kb_cb(key):
            """ Callback function which is called when a key has been pressed.
            """
            global last_key
            last_key = key.name
            
        # Tell the keyboard module to respond to these particular keys only
        keyboard.on_release_key("q", callback=kb_cb)
        keyboard.on_release_key("w", callback=kb_cb)
        keyboard.on_release_key("e", callback=kb_cb)
        keyboard.on_release_key("r", callback=kb_cb)
        
        keyboard.on_release_key("1", callback=kb_cb)
        keyboard.on_release_key("2", callback=kb_cb)
        keyboard.on_release_key("3", callback=kb_cb)
        keyboard.on_release_key("4", callback=kb_cb)
        keyboard.on_release_key("5", callback=kb_cb)
        keyboard.on_release_key("6", callback=kb_cb)
        keyboard.on_release_key("7", callback=kb_cb)
        keyboard.on_release_key("8", callback=kb_cb)
        
        # Initialize variables
        pay = 0
        chg = 0
        prod = ''
        coin = ''
        balUpd = 0
        
        # Define Price for Each Item
        product = ("Cuke","Popsi","Spryte","Dr. Pupper")
        price = (100,120,85,110)
        
        # Define Payment String Tuple for Display
        payTup = ("pennies","nickels","dimes","quarters","ones","fives","tens","twenties")
        
        state = 1
    
    
    #State 1 - Hub
    if state == 1:
        if last_key != '':
            state = 2
        elif balUpd == 1:
            state = 3
    
    #State 2 - Input Processing
    if state == 2:
        # If key press corresponds to a drink, determine what drink
        if last_key == 'q' or 'w' or 'e' or 'r':
            if last_key == 'q':
                prod = 0
            elif last_key == 'w':
                prod = 1
            elif last_key == 'e':
                prod = 2
            else:
                prod = 3
            balUpd = 1  # Raise Balance Update Flag
        
        # If key press corresponds to a coin input, determine what coin    
        if last_key == '1' or '2' or '3' or '4' or'5' or '6' or '7' or '8':
            if last_key == '1':
                coin = 0
            elif last_key == '2':
                coin = 1
            elif last_key == '3':
                coin = 2
            elif last_key == '4':
                coin = 3
            elif last_key == '5':
                coin = 4
            elif last_key == '6':
                coin = 5
            elif last_key == '7':
                coin = 6
            else:
                coin = 7
            balUpd = 1  # Raise Balance Update Flag    
                
        state = 1
    
    
    #State 3 - Balance Computation
    if state == 3:
        if coin != '':
            bal = getBalance(bal,coin)
            coin = ''
            balUpd = 0
        elif prod != '':
            chg,chgTup,suf = getChange(bal,price(prod))
            if suf == 0:
                prod = ''
            else:
                vendProd = 1
            balUpd = 0
        else:
            error = 1            
            balUpd = 0
    
        state = 1
    
    
    #State 4 - Display
    if state == 4:
        # Implement State Here
        
        state = 1
    
    
    #State 5 - Dispense Soda
    if state == 5:
        
        # This needs to be moved to DISPLAY ---------------------------------------------------
        #print("Enjoy Your " + product(prod) + "!")
        #print("Thank you, please come again!")
        
        print("-----   " + "One " + product(prod) + " dispensed" + "   -----")
        state = 1
    
    #State 6 - Dispense Change
    if state == 6:
        # This needs to be moved to DISPLAY ---------------------------------------------------
        #print("Your change is " + str(chg/100))
        
        for x in payTup:
            if chgTup(x) > 0:
                print("-----   " + str(chgTup(x)) + " " + payTup(x) + " have been dispensed" + "   -----")
        
        state = 1