# -*- coding: utf-8 -*-
"""
Created on Fri Jan 15 09:17:55 2021

@author: hallm
"""

class animal:
    '''
    '''
    myVar = 23
    
    def __init__(self, name, species, weight=0, laser_type=None):
        '''
        '''
        ## The name of the animal
        self.name = name
        
        ## The species of the animal
        self.species = species
        
        ## The weight of the animal
        self.weight = weight
        
        ## The laser the animal is equipped with
        self.laser_type = laser_type
        
    def eat(self, foodToEat):
        '''
        '''
        self.weight += 0.5
        print(foodToEat + " was tasty!")
        
    def run(self, distance):
        '''
        '''
        self.weight -= 0.1*distance
        print(self.name + " ran " + str(distance) + " miles!")
        
        
if __name__ == "__main__":
    # Create two objects to play with in the console
    myAnimal = animal('Chloe', 'Dog', weight=65)
    yourAnimal = animal('The Colonel', 'Cat')
    yourAnimal.laser_type = 'Nuclear Pulse'