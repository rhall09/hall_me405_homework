'''
@file ME405-HW1-RHall.py
@brief This script contains a function called getChange that computes change given a payment and an item cost
@detail This script contains a function called getChange that computes appropriate denominations of change
given inputs of a price (integer in cents) and the denominations of the payment (tuple containing integer numbers of
each denomination). The function outputs a corresponding tuple with the change denominations and a boolean indicating
if the payment was sufficient to cover the price. This is primarilly to differentiate exact change from insufficient
funding, both of which return a tuple of zero values. This boolean is high when payment is suficient and low when it
is not. This allows this function to be optimally utilized in a larger script. The input and output tuples will be of 
the form: (pennies, nickels, dimes, quarters, ones, fives, tens, twenties). The input may neglect upper values, but
must include all indicies lower than the minimum utilized index.
@author Rick Hall
@date 01/12/2021
'''




def getChange(price,payment):
    '''
    @brief Function to determine change given any payment denominations up to $20
    @detail This function computes the appropriate change to be returned given an
    integer representing price (in cents) and a tuple indicating the denominations
    provided for payment.
    @param price This is the price of the desired object. It is an integer in cents. For example, a $5 item would be entered as 500
    @param payment This is the payment tuple, indicating the given payment denominations. This tuple will be of the form: (0-pennies,1-nickels,2-dimes,3-quarters,4-ones,5-fives,6-tens,7-twenties) 
    @return Two returns are given - a boolean to indicate if the given payment was sufficient and a tuple containing the appropriate change. If the payment is insufficient, all elements of the return tuple will be zero.
    '''
    
    pay_indx = (1,5,10,25,100,500,1000,2000)    # Defines value of each location in payment/change lists
    
    pay_in = list(payment)    # Convert payment from a tuple to a list
    pay_out = 0               # Define payment numeral
    
    chng_out = [0,0,0,0,0,0,0,0]    # Define change list
    
    
    # Determine decimal value of payment
    pointer = len(pay_in) - 1   # Create a pointer to index relative to pay_indx. This is explicitely computed to allow for tuple inputs of reduced lengths (ex: payment = (3, 0, 5) )
    while pointer >= 0:     # Repeat until pointer has run through each location of payment list
        while pay_in[pointer] > 0:      # Repeat until location of payment tuple indicated by pointer is reduced to zero
            pay_out = pay_out + pay_indx[pointer]  
            pay_in[pointer] = pay_in[pointer] - 1   #                Decrement location of payment list
            
        pointer = pointer - 1   # Decrement pointer
        
    # From decimal payment, determine change ammount
    chng_in = pay_out - price
    
    # Determine if payment was sufficient and if change should be returned
    if chng_in > 0:
        chng_state = 1     # Payment exceeded price - Compute change
    elif chng_in == 0:
        chng_state = 2     # Payment exactly matched price - Skip computation
    else:
        chng_state = 3     # Payment insufficient - Return zero tuple and Error

   
    # Run change computation
    if chng_state == 1:
        pointer = 7    # len(pay_indx) - 1    # Reset pointer to max index of pay_inx. Preallocated for speed.
        while pointer >= 0:     # Repeat until pointer has run through each location of payment list
            while chng_in >= pay_indx[pointer]:     # Repeat until 
                chng_in = chng_in - pay_indx[pointer]        # For each loop: Decrease change numeral by ammount at pointer location of pay_indx
                chng_out[pointer] = chng_out[pointer] + 1   #                 Increment location of change list
                
            pointer = pointer - 1   # Decrement pointer
    
    # Give Return Boolean for sufficient funding
    if chng_state == 3:
        sufficient = 0
    else:
        sufficient = 1
            
    chng_out = tuple(chng_out)
    return chng_out, sufficient
            

## testing code - code block only when run as script

if __name__ == "__main__":
    print('Running test code...')
    
    chng_out,chng_state = getChange(6432,(6,1,2,2,1,1,2))
    
    print("The returned change should be:")
    print(chng_out)

    if chng_state == 0:
        print("The payment was insufficient")
    
    pass # This is a null statement - the interpreter ignores it
    # If there was a blank space instead of pass, the interpreter would throw an error
